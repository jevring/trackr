# Trackr

# Requirements
* Java 9
* Maven 3
* Docker

# Building

To build: `mvn clean install`. This will also generate the docker images.

# Running

To run: `docker run -p 8080:8080 -t trackr:latest`

After running, the service is available at `http://$docker-host:8080/ads/*`

# How would I have done this if it had been a real system?
Well, the first thing is that I would have used a real time-series database, rather than the simplistic in-memory one I have now.
There are plenty of time-series databases on the market. I'd do some research, then test the top, say, 3 candidates. I'd find the
best one w.r.t performance, maintainability, and cost. If we *absolutely* had to design something like this ourselves, we'd have 
to put a lot more effort in to it than what I'm willing to do here. It would be doable, but it would take some effort. 

I would also separate the components that *ingest* data from the ones that produce statistics. There's no point in having them in
the same application. Having them in separate applications allows them to be tuned and provisioned separately.

Assuming a decent time-series database, it should be possible to scale the ingestion system out, i.e. run more replicas as load increases.
The statistics generation, which presumably is done significantly less often, might benefit from fewer larger systems, if the data sets are very large.
There could be multiple instances of those as well, of course, but presumably you'd need fewer.