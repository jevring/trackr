Setting up the scaffolwing and stuff took me perhaps 1 hour.

Struggling with the non-standard date time stuff took me perhaps another 30 minutes.

Actually figuring out how to extract the stats is ongoing, and might require a rethink of how things are stored.
For example, will we need separate time series for each data type?
I really like this task! It's difficult, at least if you've never done it before, and it makes me think! =)
It ended up taking about an hour. I made one attempt in the group direction until I realized I could group by lists of 
strings. Once again, thanks stack overflow! What would I do without you? =)

After some testing, I'm now well in to another 30 minutes of messing around with specially formatted dates.
I don't know if this was the point of the entire exercise, but I would have rather spent my time elsewhere.
I know dates are a tricky thing, but they're made considerably more tricky by deviating from standards.
Had this been a real product, I might have challenged the choice of timestamp at design time. 

I'm now finally done. All in all I enjoyed it. It took a couple of hours. Most time wasted was in date processing.
I only wrote MockMVC tests. Coverage due to these tests is fairly high. 91% line coverage according to IntelliJ IDEA.
I could have spent extra time testing every single other thing, but to be perfectly honest the only tricky thing here
was the stats extraction. Well, the dates, but that's (hopefully) not what the exercise was about.

