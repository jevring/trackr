package net.jevring.adtech.trackr;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import net.jevring.adtech.trackr.model.Click;
import net.jevring.adtech.trackr.model.Delivery;
import net.jevring.adtech.trackr.model.Install;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext
public class TrackrApplicationTests {
	private static final DateTimeFormatter parameterFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssXXXX");
	private static final List<String> browsers = List.of("Chrome", "Firefox", "Safari", "Brave", "Opera");
	private static final List<String> oses = List.of("Windows", "Linux", "iOS", "macOS", "Android");
	private static final OffsetDateTime NOW = OffsetDateTime.now();

	@Autowired
	private MockMvc mvc;

	@Autowired
	private Jackson2ObjectMapperBuilder objectMapperBuilder;

	private ObjectMapper objectMapper;

	@Before
	public void setUp() throws Exception {
		DateTimeFormatter noColonInOffsetWithNanos = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSSSSSXXXX");
		objectMapper = objectMapperBuilder.serializerByType(OffsetDateTime.class, new JsonSerializer<OffsetDateTime>() {
			@Override
			public void serialize(OffsetDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
				gen.writeString(noColonInOffsetWithNanos.format(value));
			}
		}).build();
	}

	@Test
	public void shouldRejectClickForDeliveryThatDoesNotExist() throws Exception {
		UUID deliveryId = UUID.randomUUID();
		Click click = new Click();
		UUID clickId = UUID.randomUUID();
		click.setClickId(clickId);
		click.setDeliveryId(deliveryId);
		click.setTime(NOW.plusSeconds(99));

		mvc.perform(post("/ads/click").content(objectMapper.writeValueAsString(click)).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
		   .andExpect(status().isNotFound());
	}

	@Test
	public void shouldRejectInstallForClickThatDoesNotExist() throws Exception {
		Install install = new Install();
		install.setClickId(UUID.randomUUID());
		install.setInstallId(UUID.randomUUID());
		install.setTime(NOW.plusSeconds(99));

		mvc.perform(post("/ads/install").content(objectMapper.writeValueAsString(install)).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
		   .andExpect(status().isNotFound());
	}

	@Test
	public void shouldAccuratelyReportStatistics() throws Exception {
		generateData();

		String start = parameterFormatter.format(NOW.plusSeconds(3600));
		String end = parameterFormatter.format(NOW.plusSeconds(23 * 3600));
		mvc.perform(get("/ads/statistics").param("start", start).param("end", end))
		   .andExpect(status().isOk())
		   .andExpect(jsonPath("interval.start").value(start))
		   .andExpect(jsonPath("interval.end").value(end))
		   .andExpect(jsonPath("stats.deliveries").value(214))
		   .andExpect(jsonPath("stats.clicks").value(90))
		   .andExpect(jsonPath("stats.installs").value(47))
		   .andExpect(jsonPath("data").doesNotExist())
		   .andDo(print());
	}

	@Test
	public void shouldAccuratelyReportGroupedStatistics() throws Exception {
		generateData();

		String start = parameterFormatter.format(NOW.plusSeconds(3600));
		String end = parameterFormatter.format(NOW.plusSeconds(23 * 3600));
		mvc.perform(get("/ads/statistics").param("start", start).param("end", end).param("group_by", "os").param("group_by", "browser"))
		   .andExpect(status().isOk())
		   .andExpect(jsonPath("interval.start").value(start))
		   .andExpect(jsonPath("interval.end").value(end))
		   .andExpect(jsonPath("stats").doesNotExist())
		   .andExpect(jsonPath("data[0].fields.os").value("macOS"))
		   .andExpect(jsonPath("data[0].fields.browser").value("Safari"))
		   .andExpect(jsonPath("data[0].stats.deliveries").value(10))
		   .andExpect(jsonPath("data[0].stats.clicks").value(5))
		   .andExpect(jsonPath("data[0].stats.installs").value(3))
		   .andExpect(jsonPath("data[5].fields.os").value("Android"))
		   .andExpect(jsonPath("data[5].fields.browser").value("Firefox"))
		   .andExpect(jsonPath("data[5].stats.deliveries").value(10))
		   .andExpect(jsonPath("data[5].stats.clicks").value(5))
		   .andExpect(jsonPath("data[5].stats.installs").value(0))
		   .andExpect(jsonPath("data[10].fields.os").value("Linux"))
		   .andExpect(jsonPath("data[10].fields.browser").value("Brave"))
		   .andExpect(jsonPath("data[10].stats.deliveries").value(10))
		   .andExpect(jsonPath("data[10].stats.clicks").value(5))
		   .andExpect(jsonPath("data[10].stats.installs").value(3))
		   .andExpect(jsonPath("data").value(hasSize(22)))
		   .andDo(print());
	}

	private void generateData() throws Exception {
		List<UUID> deliveryIds = new ArrayList<>();

		int deliverySecondsOffset = 0;

		for (String browser : browsers) {
			for (String os : oses) {
				Delivery delivery = new Delivery();
				delivery.setBrowser(browser);
				delivery.setOs(os);
				for (int i = 0; i < 10; i++) {
					UUID deliveryId = UUID.randomUUID();
					deliveryIds.add(deliveryId);
					delivery.setDeliveryId(deliveryId);
					delivery.setTime(NOW.plusSeconds(deliverySecondsOffset += 100).plusNanos(1));
					delivery.setAdvertisementId(1);
					delivery.setSite("http://test.com");

					mvc.perform(post("/ads/delivery").content(objectMapper.writeValueAsString(delivery)).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					   .andExpect(status().isOk());
				}
			}
		}

		int clicksSecondsOffset = 0;
		List<UUID> clickIds = new ArrayList<>();
		for (int i = 0; i < deliveryIds.size(); i++) {
			if (i % 2 == 0) {
				continue;
			}
			UUID deliveryId = deliveryIds.get(i);
			Click click = new Click();
			UUID clickId = UUID.randomUUID();
			clickIds.add(clickId);
			click.setClickId(clickId);
			click.setDeliveryId(deliveryId);
			click.setTime(NOW.plusSeconds(clicksSecondsOffset += 101).plusNanos(1));

			mvc.perform(post("/ads/click").content(objectMapper.writeValueAsString(click)).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
			   .andExpect(status().isOk());
		}

		int installSecondsOffset = 0;
		for (int i = 0; i < clickIds.size(); i++) {
			if (i % 3 == 0) {
				continue;
			}
			UUID clickId = clickIds.get(i);
			Install install = new Install();
			install.setClickId(clickId);
			install.setInstallId(UUID.randomUUID());
			install.setTime(NOW.plusSeconds(installSecondsOffset += 99).plusNanos(1));

			mvc.perform(post("/ads/install").content(objectMapper.writeValueAsString(install)).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
			   .andExpect(status().isOk());
		}
	}

}
