package net.jevring.adtech.trackr.exceptions;

/**
 * @author markus@jevring.net
 */
public class NotFoundException extends Exception {
	public NotFoundException(String message) {
		super(message);
	}
}
