package net.jevring.adtech.trackr.controllers;

import net.jevring.adtech.trackr.exceptions.NotFoundException;
import net.jevring.adtech.trackr.model.*;
import net.jevring.adtech.trackr.service.TrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author markus@jevring.net
 */
@RestController
@RequestMapping("/ads")
public class AdsController {
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssX");
	private final TrackingService trackingService;

	@Autowired
	public AdsController(TrackingService trackingService) {
		this.trackingService = trackingService;
	}

	@PostMapping("delivery")
	public void delivery(@RequestBody Delivery delivery) {
		trackingService.registerDelivery(delivery);
	}

	@PostMapping("click")
	public void click(@RequestBody Click click) throws NotFoundException {
		trackingService.registerClick(click);
	}

	@PostMapping("install")
	public void install(@RequestBody Install install) throws NotFoundException {
		trackingService.registerInstall(install);
	}

	/**
	 * Due to the non-standard "+0000" timezone offset, which doesn't include the colon separator between the hours
	 * and the minutes, we have to parse stuff ourselves. Thanks, stack overflow! =)
	 * It would have been nice to just specify a StatisticsRequest as a parameter to this method, but it's too cumbersome with
	 * non-standard date parsing. It's always the dates... =(
	 */
	@GetMapping(path = "statistics", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Statistics statistics(@RequestParam String start,
	                             @RequestParam String end,
	                             @RequestParam(name = "group_by", required = false) List<String> groupBy) {
		OffsetDateTime startTime = OffsetDateTime.parse(start, formatter);
		OffsetDateTime endTime = OffsetDateTime.parse(end, formatter);
		return trackingService.generateStatistics(Interval.builder().start(startTime).end(endTime).build(), groupBy);
	}

}
