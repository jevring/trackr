package net.jevring.adtech.trackr.controllers;

import lombok.extern.slf4j.Slf4j;
import net.jevring.adtech.trackr.exceptions.NotFoundException;
import net.jevring.adtech.trackr.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author markus@jevring.net
 */
@Slf4j
@RestControllerAdvice
public class ExceptionAdvice {

	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ErrorResponse handleNotFoundException(NotFoundException exception) {
		return ErrorResponse.builder().message(exception.getMessage()).build();
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorResponse handleException(Exception exception) {
		log.error("Request failed", exception);
		return ErrorResponse.builder().message(exception.getMessage()).build();
	}
}
