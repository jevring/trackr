package net.jevring.adtech.trackr.service;

import net.jevring.adtech.trackr.exceptions.NotFoundException;
import net.jevring.adtech.trackr.model.*;
import net.jevring.adtech.trackr.service.tsdb.TimeSeriesDatabase;
import net.jevring.adtech.trackr.service.tsdb.TimestampedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author markus@jevring.net
 */
@Service
public class TrackingService {
	private final TimeSeriesDatabase<OffsetDateTime, TimestampedEvent<OffsetDateTime>> timeSeriesDatabase;
	private final ConcurrentMap<UUID, Delivery> deliveries = new ConcurrentHashMap<>();
	private final ConcurrentMap<UUID, Click> clicks = new ConcurrentHashMap<>();

	@Autowired
	public TrackingService(TimeSeriesDatabase<OffsetDateTime, TimestampedEvent<OffsetDateTime>> timeSeriesDatabase) {
		this.timeSeriesDatabase = timeSeriesDatabase;
	}

	public void registerDelivery(Delivery delivery) {
		deliveries.put(delivery.getDeliveryId(), delivery);
		timeSeriesDatabase.registerEvent(delivery);
	}

	public void registerClick(Click click) throws NotFoundException {
		assertDeliveryExists(click.getDeliveryId());
		clicks.put(click.getClickId(), click);
		timeSeriesDatabase.registerEvent(click);
	}

	public void registerInstall(Install install) throws NotFoundException {
		assertClickExists(install.getClickId());
		timeSeriesDatabase.registerEvent(install);
	}

	private void assertDeliveryExists(UUID deliveryId) throws NotFoundException {
		if (!deliveries.containsKey(deliveryId)) {
			throw new NotFoundException(String.format("Unknown delivery: %s", deliveryId));
		}
	}

	private void assertClickExists(UUID clickId) throws NotFoundException {
		if (!clicks.containsKey(clickId)) {
			throw new NotFoundException(String.format("Unknown click: %s", clickId));
		}
	}

	public Statistics generateStatistics(Interval interval, List<String> groupBy) {
		// NOTE: as the time series database is concurrently being updated, we might see an incoherent view of the data.
		// for example, by the time we get to extracting the installs, more installs may have arrived for that time range.
		// this could be solvable to a certain extent by taking a snapshot of the data, but even taking a snapshot isn't
		// atomic (unless you lock, but we don't want to do that), so you would just shrink the window.
		// whether or not this is relevant is unknown. If it is important, we would have to choose a different solution, 
		// with different trade-offs
		Collection<TimestampedEvent<OffsetDateTime>> events = timeSeriesDatabase.getEvents(interval.getStart(), interval.getEnd());
		List<Delivery> deliveries = extractOfType(events, Delivery.class);
		List<Click> clicks = extractOfType(events, Click.class);
		List<Install> installs = extractOfType(events, Install.class);
		if (groupBy == null || groupBy.isEmpty()) {
			Stats stats = Stats.builder().deliveries(deliveries.size()).clicks(clicks.size()).installs(installs.size()).build();
			return Statistics.builder().interval(interval).stats(stats).build();
		} else {
			List<GroupedData> data = new ArrayList<>();

			// group by a list of values out of the delivery, then fetch (from the clicks and installs in the time range) the
			// corresponding clicks and installs. Finally, once each sub-selection is done, could the deliveries, clicks and 
			// installs, and generate the statistics group.
			Map<List<String>, List<Delivery>> groups = deliveries.stream().collect(Collectors.groupingBy(classifier(groupBy)));
			for (Map.Entry<List<String>, List<Delivery>> entry : groups.entrySet()) {
				List<String> keys = entry.getKey();
				List<Delivery> deliveriesInGroup = entry.getValue();
				Set<UUID> deliveryIdsInGroup = deliveriesInGroup.stream().map(Delivery::getDeliveryId).collect(Collectors.toSet());

				List<Click> clicksInGroup = clicks.stream().filter(click -> deliveryIdsInGroup.contains(click.getDeliveryId())).collect(Collectors.toList());
				Set<UUID> clickIdsInGroup = clicksInGroup.stream().map(Click::getClickId).collect(Collectors.toSet());

				List<Install> installsInGroup =
						installs.stream().filter(install -> clickIdsInGroup.contains(install.getClickId())).collect(Collectors.toList());

				Stats stats = Stats.builder().deliveries(deliveriesInGroup.size()).clicks(clicksInGroup.size()).installs(installsInGroup.size()).build();

				Map<String, String> fields = new HashMap<>();
				for (int i = 0; i < groupBy.size(); i++) {
					String groupType = groupBy.get(i);
					String groupValue = keys.get(i);
					fields.put(groupType, groupValue);
				}
				data.add(GroupedData.builder().fields(fields).stats(stats).build());
			}
			return Statistics.builder().interval(interval).data(data).build();
		}
	}

	private Function<Delivery, List<String>> classifier(List<String> groupBy) {
		return delivery -> groupBy.stream().map(group -> getGroupByValue(delivery, group)).collect(Collectors.toList());
	}

	private String getGroupByValue(Delivery delivery, String group) {
		switch (group) {
			case "browser":
				return delivery.getBrowser();
			case "site":
				return delivery.getSite();
			case "os":
				return delivery.getOs();
			case "advertisementId":
				return String.valueOf(delivery.getAdvertisementId());
			default:
				throw new IllegalArgumentException("Unknown group_by field");
		}
	}

	private <T> List<T> extractOfType(Collection<TimestampedEvent<OffsetDateTime>> events, Class<T> clazz) {
		// not sure if instanceof checks are the most elegant thing to do here
		return events.stream().filter(clazz::isInstance).map(clazz::cast).collect(Collectors.toList());
	}
}
                             