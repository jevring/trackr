/**
 * This package contains the time series database.
 * As this is just a trivial example, it'll only ship with an in-memory tsdb.
 * For a real deployment, we'd provide an interface to some external time series
 * database that would persist this kind of data, and scale, etc.
 *
 * @author markus@jevring.net
 */
package net.jevring.adtech.trackr.service.tsdb;