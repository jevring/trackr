package net.jevring.adtech.trackr.service.tsdb;

import java.time.temporal.TemporalAccessor;
import java.util.Collection;

/**
 * @author markus@jevring.net
 */
public interface TimeSeriesDatabase<K extends TemporalAccessor, V extends TimestampedEvent<K>> {
	void registerEvent(V event);

	Collection<V> getEvents(K start, K end);
}
