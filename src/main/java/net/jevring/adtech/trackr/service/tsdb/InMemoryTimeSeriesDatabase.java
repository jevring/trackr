package net.jevring.adtech.trackr.service.tsdb;

import org.springframework.stereotype.Service;

import java.time.temporal.TemporalAccessor;
import java.util.Collection;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * @author markus@jevring.net
 */
@Service
public class InMemoryTimeSeriesDatabase<K extends TemporalAccessor, V extends TimestampedEvent<K>> implements TimeSeriesDatabase<K, V> {
	private final ConcurrentNavigableMap<K, V> timeSeries = new ConcurrentSkipListMap<>();

	@Override
	public void registerEvent(V event) {
		timeSeries.put(event.getTime(), event);
	}

	@Override
	public Collection<V> getEvents(K start, K end) {
		return timeSeries.subMap(start, true, end, true).values();
	}
}
