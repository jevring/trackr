package net.jevring.adtech.trackr.service.tsdb;

import java.time.temporal.TemporalAccessor;

/**
 * @author markus@jevring.net
 */
public interface TimestampedEvent<T extends TemporalAccessor> {
	public T getTime();
}
