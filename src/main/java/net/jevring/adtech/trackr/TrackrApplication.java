package net.jevring.adtech.trackr;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

@SpringBootApplication
public class TrackrApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrackrApplication.class, args);
	}

	@Autowired
	public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder objectMapperBuilder) {
		DateTimeFormatter noColonInZoneOffsetWithMillis = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSSSSSXXXX");
		objectMapperBuilder.deserializerByType(OffsetDateTime.class, new JsonDeserializer<OffsetDateTime>() {
			@Override
			public OffsetDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
				return OffsetDateTime.parse(p.getValueAsString(), noColonInZoneOffsetWithMillis);
			}
		});
		DateTimeFormatter noColonInOffset = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssXXXX");
		objectMapperBuilder.serializerByType(OffsetDateTime.class, new JsonSerializer<OffsetDateTime>() {
			@Override
			public void serialize(OffsetDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
				gen.writeString(noColonInOffset.format(value));
			}
		});
		return objectMapperBuilder.build();

	}
}
