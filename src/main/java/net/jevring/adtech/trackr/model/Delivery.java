package net.jevring.adtech.trackr.model;

import lombok.Data;
import net.jevring.adtech.trackr.service.tsdb.TimestampedEvent;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * @author markus@jevring.net
 */
@Data
public class Delivery implements TimestampedEvent<OffsetDateTime> {
	private long advertisementId;
	private UUID deliveryId;
	private OffsetDateTime time;
	private String browser;
	private String os;
	private String site;
}
