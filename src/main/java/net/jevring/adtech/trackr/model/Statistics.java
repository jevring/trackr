package net.jevring.adtech.trackr.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Value;

import java.util.List;

/**
 * @author markus@jevring.net
 */
@Value
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Statistics {
	private final Interval interval;
	private final Stats stats;
	private final List<GroupedData> data;

}
