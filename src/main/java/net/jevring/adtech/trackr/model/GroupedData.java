package net.jevring.adtech.trackr.model;

import lombok.Builder;
import lombok.Value;

import java.util.Map;

/**
 * @author markus@jevring.net
 */
@Value
@Builder
public class GroupedData {
	private final Map<String, String> fields;
	private final Stats stats;
}
