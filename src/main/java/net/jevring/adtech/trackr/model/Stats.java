package net.jevring.adtech.trackr.model;

import lombok.Builder;
import lombok.Value;

/**
 * @author markus@jevring.net
 */
@Value
@Builder
public class Stats {
	private final long deliveries;
	private final long clicks;
	private final long installs;
}
