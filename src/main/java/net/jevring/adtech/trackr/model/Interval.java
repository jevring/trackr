package net.jevring.adtech.trackr.model;

import lombok.Builder;
import lombok.Value;

import java.time.OffsetDateTime;

/**
 * @author markus@jevring.net
 */
@Value
@Builder
public class Interval {
	private final OffsetDateTime start;
	private final OffsetDateTime end;
}
