package net.jevring.adtech.trackr.model;

import lombok.Data;
import net.jevring.adtech.trackr.service.tsdb.TimestampedEvent;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * @author markus@jevring.net
 */
@Data
public class Click implements TimestampedEvent<OffsetDateTime> {
	private UUID deliveryId;
	private UUID clickId;
	private OffsetDateTime time;
}
