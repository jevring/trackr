package net.jevring.adtech.trackr.model;

import lombok.Builder;
import lombok.Value;

/**
 * @author markus@jevring.net
 */
@Value
@Builder
public class ErrorResponse {
	private final String message;
}
